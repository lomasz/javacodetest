package task3;

import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Task3 {

	public static void main(String[] args) {
		
		int year = 2009;
		
		//Load & Write Data File from URL link
		Scanner sourceFile = readSourceFile("http://www.nhc.noaa.gov/data/hurdat/hurdat2-nepac-1949-2015-050916.txt");
		
		//Create Hurricane List
		ArrayList<Hurricane> hurricaneList = getHurricaneInfoForYearFromFile(sourceFile, year);
			
		//Print Hurricane List (storm name and maximum sustained windspeed in knots)
		printHurricaneList(hurricaneList);
		
	}
	
	public static Scanner readSourceFile(String urlString){
		
		Scanner scanner = null;
		
		try {
			
			URL url = new URL(urlString);
			
			scanner = new Scanner(url.openStream());
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		return scanner;
	}

	public static ArrayList<Hurricane> getHurricaneInfoForYearFromFile(Scanner sourceFile, int year){
		
		//Regular expressions
		
		//Header: (EP) - only Northeast Pacific, (CP) � only North Central Pacific, (EP|EC) - both
		Pattern headerRegex = Pattern.compile("(EP)(\\d)(\\d)(" + year + ").*?((?:[a-z][a-z0-9_]*)).*?(\\d+)", Pattern.CASE_INSENSITIVE);
		
		//Data Line: Maximum sustained wind (in knots) - Spaces 39-41, before 7th comma
		Pattern dateLinesRegex = Pattern.compile(".*?\\d+.*?\\d+.*?\\d+.*?\\d+.*?\\d+.*?\\d+.*?(\\d+)", Pattern.CASE_INSENSITIVE);
		
		Matcher headerMatcher = null;
		Matcher dateLinesMatcher = null;
		String line = null;
				
		ArrayList<Hurricane> hurricaneList = new ArrayList<Hurricane>(); 
			
		while (sourceFile.hasNextLine()) {
				
			line = sourceFile.nextLine();
					
			headerMatcher = headerRegex.matcher(line);
					
			if (headerMatcher.find()){
						
				String hurricaneName = headerMatcher.group(5);
						
				int numberOfDateLines = Integer.parseInt(headerMatcher.group(6));
						
				Integer maxSustainedWindspeed = 0;
						
				for (int i = 0; i < numberOfDateLines; i++){
							
					line = sourceFile.nextLine();
							
					dateLinesMatcher = dateLinesRegex.matcher(line);
							
					if (dateLinesMatcher.find()){
								
						maxSustainedWindspeed = Math.max(maxSustainedWindspeed, Integer.parseInt(dateLinesMatcher.group(1)));
								
					}		
							
				}
						
				hurricaneList.add(new Hurricane(hurricaneName, maxSustainedWindspeed));
						
			}
					
		}
				
		sourceFile.close();
		
		return hurricaneList;
	
	}
	
	public static void printHurricaneList(ArrayList<Hurricane> list){
		
		for (Hurricane hurricane: list) {
			
			System.out.printf("%-19s%3d\n", hurricane.getName(), hurricane.getMaxSustainedWindspeed() );
				
		}
			
	}
	
}

