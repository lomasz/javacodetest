package task3;

public class Hurricane {
	
	private String name;
	private int maxSustainedWindspeed;

	public String getName() {
		
		return name;
		
	}

	public void setName(String name) {
		
		this.name = name;
		
	}

	public int getMaxSustainedWindspeed() {
		
		return maxSustainedWindspeed;
		
	}

	public void setMaxSustainedWindspeed(int maxSustainedWindspeed) {
		
		this.maxSustainedWindspeed = maxSustainedWindspeed;
		
	}

	public Hurricane(String name, int maxSustainedWindspeed) {

		this.name = name;
		
		this.maxSustainedWindspeed = maxSustainedWindspeed;
		
	}

}
