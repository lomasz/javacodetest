package task2;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import task2.Data.type;

public class Task2 {

	public static void main(String[] args) throws IOException, ParseException {
		
		//Connect to HTML Page with data 
        Connection connect = Jsoup.connect("http://www.mercado.ren.pt/EN/Electr/MarketInfo/Interconnections/CapForecast/Pages/Daily.aspx");
        Document document = connect.get();
        
        //Create Date List
        List<String> dateList = createDateList(document);
   
        //Create Data Array
        ArrayList<List<Data>> dataArray = createDataArray(document);
        
        //Calculating sum of Forecast and Actual Values group by Day
        int[][] sumArray = sumValues(dataArray);
        
        printValues(sumArray, dateList);
        
	}
	
	
	public static type decodeDataType(String className){
		/*
		 * Function decoding HTML/XML element's attributes to enum Data.type
		 * input:	HTML class name (String)
		 * return:	Data.type (enum)
		 */
		
		if (className.equals("txtPREV") || className.equals("txtrPREV")) {
				
			return type.FORECAST;
				
		} else if (className.equals("txtVERIF") || className.equals("txtrVERIF")) {
			
			return type.ACTUAL;
			
		}
		
		return null;
		
	}

	public static Date convertStringToDate(String date) throws ParseException{
		/*
		 * Function converts Date String to Date type
		 * input:	Date String (example: 20 Jan, 2 Feb)
		 * returns:	Date
		 */
		
		date = date.replace(" ", "-"); //weird space from HTML source document
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		
		return format.parse(date);
		
	}
	
	public static String convertDateToString(Date date) throws ParseException{
		/*
		 * Function converts Date to String (yyyy-MM-dd)
		 * input:	Date
		 * returns:	Date String (yyyy-MM-dd)
		 */
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		return format.format(date);
		
	}

	public static List<String> createDateList(Document document) throws ParseException{
		/*
		 * Function creates Array List with dates from header table.
		 * Input: HTML Page (Jsoup's Document)
		 * returns: Date Array List (List of Strings)
		 */
	
		 //Select dates from table's header on HTML page
        Elements headerList = document.select("th.rbcellBDRCOLOR, th.bcellBDRCOLOR");
        
        //Remove first value from dates "HOUR"
        headerList.remove(0);
        
        List<String> dateList = new ArrayList<String>();
        
        int year = Calendar.getInstance().get(Calendar.YEAR);
        
        Date date = new Date();
        Date tempDate = convertStringToDate(headerList.get(0).text() + "-" + year);

        for(Element cell: headerList){
        	
        	date = convertStringToDate(cell.text() + "-" + year);
        	
        	if (tempDate.after(date)){
        		
        		date = convertStringToDate(cell.text() + "-" + (year + 1));       		
        		
        	}
        	
        	dateList.add(convertDateToString(date));
        	
        	tempDate = date;
        
        }
        
        return dateList;
		
	}

	public static ArrayList<List<Data>> createDataArray(Document document){
		/*
		 * Function creates 2-dimensional List of Data's Objects (data from table)
		 * Input: HTML Page (Jsoup's Document)
		 * returns: Date Array List (List of Data's class objects)
		 */
		
        //Select data from table's rows on HTML page
        Elements dataRows = document.select("tr.trIMPAR, tr.trPAR");
        
        //Create Data's Array - size of array based: number of days in tables x number of data rows (24 h)
        ArrayList<List<Data>> dataArray = new ArrayList<List<Data>>();
        
        //LOOP - for each table's row with Hourly data
        for(Element dataRow: dataRows) {
        	
        	//Get children of row's Element - cells with data
        	Elements row = dataRow.children();
        	
        	//Remove first cell - Number of hour
        	row.remove(0);
        	
        	List<Data> tempArray = new ArrayList<Data>();
        	
        	//LOOP - for each cell get value and data type (Actual/Forecast), convert to Data Class Object in DataArray
        	for (Element cell: row){
        	
        		tempArray.add(new Data(Integer.parseInt(cell.text()), decodeDataType(cell.attr("class"))));
        		
        		//System.out.println("[" + columnNumber  + "][" + rowNumber + "] =	" + dataArray[columnNumber][rowNumber].getValue() + "	(" + dataArray[columnNumber][rowNumber].getType() + ")");
        	
        	}
        	
        	dataArray.add(tempArray);
        	
        }   
        
         return dataArray;
		
	}
	
	public static int[][] sumValues(ArrayList<List<Data>> dataArray){
		/*
		 * Function creates 2-dimensional Sum of Actual/Forecast
		 * Input: 2-dimensional Data's Array (ArrayList<List<Data>>)
		 * returns: 2-dimensional Integer's Array:
		 * 	- 1st column: Daily Sum of Actual Values
		 *  - 2nd column: Daily Sum of Forecast Values
		 */
		
		int[][] sumArray = new int[dataArray.get(0).size()][2]; 
        
        //Fill array with 0
        for (int[] row: sumArray){
        	
            Arrays.fill(row, 0);
            
        }
        
        for (List<Data> subArray :  dataArray){
        	
        	Integer iterator = 0;
        	
        	for (Data data : subArray){
        		
        		switch (data.getType()) {
        		
                case ACTUAL:
                	
                	sumArray[iterator][0] += data.getValue();
                	break;
                	
                case FORECAST:
                	
                	sumArray[iterator][1] += data.getValue();
                	break;
        		
        		}
        		
        		iterator++;
        	
        	}
        
        }
        
        return sumArray;
		
	}

	public static void printValues(int[][] summary, List<String> dates){
		/*
		 * Function prints in console in two lines values daily:
		 * 	- 1st line: Sum of Forecast Values
		 * 	- 2st line: Sum of Actual Values
		 */
		
		for (int i = 0; i < summary.length; i++){
        	
	        //Forecast: yyyy-MM-dd: <sum which you need to calculate>
	        System.out.println("Forecast:\t" + dates.get(i) + ":\t" + summary[i][1]);
	        	
	        //Actual: yyyy-MM-dd: <sum which you need to calculate>
	        System.out.println("Actual:\t\t" + dates.get(i) + ":\t" + summary[i][0]);
	        	
		}

	}
	
}
