package task2;

public class Data {
	
	Integer value;
	Data.type type;

	public Data(Integer value, Data.type type) {
		
		this.value = value;
		this.type = type;
		
	}
	
	public Integer getValue(){
		
		return this.value;
		
	}
	
	public Data.type getType(){
		
		return this.type;
		
	}
	
	@Override
	public String toString(){
	         
		return this.getValue() + " (" +this.getType() + ")";
	
	}
	
	public enum type {
		
        ACTUAL,
        FORECAST;
		
    }
	

}
