package task4;

import java.util.Scanner; 

public class Task4 {

    public static void main(String[] args) {
	
		if (args.length == 0) {
		
			Scanner reader = new Scanner(System.in);
			
			System.out.println("Enter a Earthlings Word: ");

			String word = reader.nextLine();
			
			System.out.println(translatesEarthlingsWords2Martians(word));
		
		} else {
	
			for(int i = 0; i < args.length; i++) {
			
				System.out.println(translatesEarthlingsWords2Martians(args[i]));
				
			}
			
		}
    }
	
	public static String translatesEarthlingsWords2Martians(String earthlingsWord) {
		/*
		 * Function translates Earthlings Words to Martians
		 * input:	Earthlings Word (String)
		 * returns:	Martians Word (String)
		 */
		
        if (earthlingsWord.length() <= 1) {
		
            return earthlingsWord;
        }

        String martiansWord = translatesEarthlingsWords2Martians(earthlingsWord.substring(1)) + earthlingsWord.charAt(0);

        return martiansWord;
		
	}
}

