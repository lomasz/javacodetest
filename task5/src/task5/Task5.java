package task5;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class Task5 {
	
	public static boolean isWeekDay(Date date){
	
		int dayOfWeek = date.getDay();
		
		return (dayOfWeek == 0 || dayOfWeek == 6);

	}

	public static List<Date> findWeekDays(Date startDate, Date endDate) {
		
		List<Date> weekDaysList = new ArrayList<Date>();
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(startDate);

		while (!endDate.before(cal.getTime())){
			
			if (isWeekDay(cal.getTime()) ==  false) {
				
				weekDaysList.add(cal.getTime());
				
			}
			
			cal.add(Calendar.DATE, 1);
			
		}

		return weekDaysList;
		
	}


	public static void main(String[] args) {
		
		/*
		 * Test
		 */
		
		Date endDate = new Date();
		Date startDate = new Date();
		startDate.setDate(1);
		endDate.setDate(10);
		
		System.out.println(endDate.getYear() + " " + endDate.getMonth() + " " + endDate.getDate());
		System.out.println(startDate.getYear() + " " + startDate.getMonth() + " " + startDate.getDate());
		
		System.out.println(findWeekDays(startDate, endDate));
		
	}

}
